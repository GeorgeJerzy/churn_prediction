import numpy as np
from scipy.optimize import curve_fit

def tanh(x, p0, p1, p2, p3):
    """
    Tanh function, 4 params.
    """
    return p3+p0*0.5*np.tanh(p2*(x-p1))


def linear(x, slope, offset):
    """
    Linear function, 2 params.
    """
    return slope*x+offset

def relu(x, p0, p1, p2, p3):
    """
    Smooth ReLU function, 4 params.
    """
    return p2+p3*np.log(1+np.exp(p0*(x-p1)))


def fit_df(df, f):
    """
    Fits function f to data given in df.
    Returns popt - optimal fit parameters, pcov - covarinace matrix of the fit, r_squared - R2 of the fit.
    
    :param df: pd.series, data to which function f is fitted,
    :param f: fit function,
    :return popt, pcov, r_squared: list of floats, parameters of the fit, if fitting is not successful than r2=-1.
    """
    xdata = np.arange(len(df))
    ydata = np.array(df)
    try:
        popt, pcov = curve_fit(f, xdata, ydata)
        residuals = ydata- f(xdata, *popt)
        ss_res = np.sum(residuals**2)
        ss_tot = np.sum((ydata-np.mean(ydata))**2)
        r_squared = 1 - (ss_res / ss_tot)
    except:
        popt = [0]
        pcov = [0]
        r_squared = -1
    return popt, pcov, r_squared   

fit_functions = {"tanh": tanh, "linear": linear, "relu": relu}

def fit_all(df, fit_functions):
    """
    Fits all fit functions to data.
    
    :param df: pd.series, data to which functions are fitted,
    :param fit_functions: dictionary, {name of fit function, fit function},
    :return fit_params: dictionary, {name of fit function, parameters of the fit}.
    """
    fit_params = {}
    for name, ff in fit_functions.items():
        popt, pcov, r_squared = fit_df(df, ff)
        fit_params[name] = [r_squared, popt, pcov]
    return fit_params